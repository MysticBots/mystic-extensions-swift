//
//  UIColor.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 20/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension UIColor {
    
    // MARK: - Class Initializers
    public convenience init(progress : CGFloat) {
        self.init(hue: progress * 0.4, saturation: 0.9, brightness: 0.9, alpha: 1.0)
    }
    
    // MARK: - Class Methods
    public static func color(progress : CGFloat) -> UIColor {
        return UIColor(progress: progress)
    }
}
