//
//  UINavigationController.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

public extension UINavigationController {
    
    public func contains(_ viewController: UIViewController) -> Bool {
        for controller : UIViewController in self.viewControllers {
            if (controller.isKind(of: type(of: viewController))) {
                return controller.isEqual(viewController);
            }
        }
        return false;
    }
    
    public func push(_ viewController: UIViewController, animationTransition animation: UIViewAnimationTransition, animationCurve curve: UIViewAnimationCurve, duration: TimeInterval) {
        
        let _duration = (duration > 0) ? duration : 0.75;
        
        UIView.animate(withDuration: _duration, animations: {
            UIView.setAnimationCurve(curve);
            self.pushViewController(viewController, animated: false);
            UIView.setAnimationTransition(animation, for: self.view, cache: false);
        })
    }
    
    public func pop(_ viewController: UIViewController, animationTransition animation: UIViewAnimationTransition, animationCurve curve: UIViewAnimationCurve, duration: TimeInterval) {
        
        let _duration = (duration > 0) ? duration : 0.75;
        
        UIView.animate(withDuration: _duration, animations: {
            UIView.setAnimationCurve(curve);
            self.popToViewController(viewController, animated: false);
            UIView.setAnimationTransition(animation, for: self.view, cache: false);
        })
    }
}
