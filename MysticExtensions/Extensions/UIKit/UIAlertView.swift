//
//  UIAlertView.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import UIKit

public extension UIAlertView {
    public static func display(_ title: String?) -> UIAlertView {
        return self.display(title, message: nil, cancelButtonTitle: nil,
                                 delegate: nil, hideAfter: 0);
    }
    
    public static func display(_ title: String?, message: String?) -> UIAlertView {
        return self.display(title, message: message, cancelButtonTitle: nil, delegate: nil, hideAfter: 0);
    }
    
    public static func display(_ title: String?, cancelButtonTitle: String?) -> UIAlertView {
        return self.display(title, message: nil, cancelButtonTitle: cancelButtonTitle, delegate: nil, hideAfter: 0);
    }
    
    public static func display(_ title: String?, message: String?, cancelButtonTitle: String?) -> UIAlertView {
        return self.display(title, message: message, cancelButtonTitle: cancelButtonTitle, delegate: nil, hideAfter: 0);
    }
    
    public static func display(_ title: String?, message: String?, cancelButtonTitle: String?, delegate: UIAlertViewDelegate?) -> UIAlertView {
        return self.display(title, message: message, cancelButtonTitle: cancelButtonTitle, delegate: delegate, hideAfter: 0);
    }
    
    public static func display(_ title: String?, hideAfter time: TimeInterval) -> UIAlertView {
        return self.display(title, message: nil, cancelButtonTitle: nil, delegate: nil, hideAfter: time);
    }
    
    public static func display(_ title: String?, message: String?, hideAfter time: TimeInterval) -> UIAlertView {
        return self.display(title, message: message, cancelButtonTitle: nil, delegate: nil, hideAfter: time);
    }
    
    public static func display(_ title: String?, message: String?, cancelButtonTitle: String?, delegate: UIAlertViewDelegate?, hideAfter time: TimeInterval) -> UIAlertView {
        
        let alert = UIAlertView(title: title, message: message, delegate: delegate, cancelButtonTitle: cancelButtonTitle);
        DispatchQueue.main.async(execute: {
            alert.show();
            if time > 0 {
                let duration : Int64 = Int64(time * Double(NSEC_PER_SEC));
                DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + Double(duration) / Double(NSEC_PER_SEC), execute: {
                    alert.dismiss(withClickedButtonIndex: alert.cancelButtonIndex, animated: true);
                });
            }
        })
        return alert;
    }
}
