//
//  Array.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension Array where Element: Equatable {
    
    // MARK: - Mutating Instance Methods
    // Remove first collection element that is equal to the given `object`:
    mutating func remove(_ object: Element) {
        if let index = self.index(of: object) {
            self.remove(at: index)
        }
    }
    
    // MARK: - Instance Methods
    func sorted<T>(withAttribute key: String?, ascending: Bool, ignoreCase: Bool) -> [T] {
        let array = self as NSArray
        if (key == nil || key!.isEmpty) {
            return array.sortedArray(using: #selector(NSString.localizedCaseInsensitiveCompare(_:)))  as! [T]
        }
        
        var sortDescriptor : NSSortDescriptor
        if (ignoreCase) {
            sortDescriptor = NSSortDescriptor(key: key, ascending: ascending,
                                              selector: #selector(NSString.localizedCaseInsensitiveCompare(_:)))
        } else {
            sortDescriptor = NSSortDescriptor(key: key, ascending: ascending)
        }
        
        return array.sortedArray(using: [sortDescriptor]) as! [T]
    }
}
