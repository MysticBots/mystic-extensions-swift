//
//  Double.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension Double {
    
    // MARK: - Initializers
    public init(_ boolValue: Bool) {
        self = NSNumber(value: boolValue).doubleValue
    }
}
