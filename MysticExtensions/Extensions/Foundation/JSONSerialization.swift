//
//  JSONSerialization.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension JSONSerialization {
    
    // MARK: - Class Methods
    public static func jsonObject(from jsonString: String) throws -> Any? {
        if let jsonData = jsonString.data(using: .utf8, allowLossyConversion: true) {
            return try JSONSerialization.jsonObject(with: jsonData, options: .allowFragments)
        }
        return nil
    }
    
    public static func jsonString(from jsonObject: Any) -> String? {
        
        guard JSONSerialization.isValidJSONObject(jsonObject) else {
            return nil
        }
        
        do {
            if let jsonData = try self.jsonData(from: jsonObject) {
                return String(data: jsonData, encoding: .utf8)
            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
        
        return nil
    }
    
    public static func jsonData(from jsonObject: Any) throws -> Data? {
        guard JSONSerialization.isValidJSONObject(jsonObject) else {
            return nil
        }
        
        return try JSONSerialization.data(withJSONObject: jsonObject, options: .prettyPrinted)
        
    }
}
