//
//  Date.swift
//  MysticExtensions
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension Date {
    
    // MARK: - Instance Methods
    public func timeString(_ format: String) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = format
        return formatter.string(from: self)
    }

}
