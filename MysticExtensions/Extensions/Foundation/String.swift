//
//  String.swift
//  iFocusTour
//
//  Created by Muneeb Ahmed Anwar on 18/09/2016.
//  Copyright © 2016 MysticBots. All rights reserved.
//

import Foundation

public extension String {
    
    // MARK: - Class Methods
    public static func random(_ length: Int) -> String {
        
        let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        let len = UInt32(letters.length)
        
        var randomString = ""
        
        for _ in 0 ..< length {
            let rand = arc4random_uniform(len)
            var nextChar = letters.character(at: Int(rand))
            randomString += NSString(characters: &nextChar, length: 1) as String
        }
        
        return randomString
    }
    
    // MARK: - Instance Methods
    public func percentEscaped() -> String {
        return CFURLCreateStringByAddingPercentEscapes(nil, self as CFString, nil, "!*'();:@&=+$,/?%#[]" as CFString, CFStringBuiltInEncodings.UTF8.rawValue) as String
    }
    
    public func isValidEmail() -> Bool {
        let stricterFilterString = "^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$"
        let emailTest = NSPredicate(format: "SELF MATCHES %@", stricterFilterString)
        return emailTest.evaluate(with: self)
    }
    
    public func contains(_ subString: String, ignoreCase ignore: Bool) -> Bool {
        
        if (ignore) {
            return self.lowercased().range(of: subString.lowercased()) != nil
        }
        
        return self.range(of: subString) != nil
    }
}
