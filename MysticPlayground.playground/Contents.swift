//: Playground - noun: a place where people can play

import MysticExtensions

func random(_ length: Int) -> String {
    
    let letters : NSString = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
    let len = UInt32(letters.length)
    
    var randomString = ""
    
    for _ in 0 ..< length {
        let rand = arc4random_uniform(len)
        var nextChar = letters.character(at: Int(rand))
        randomString += NSString(characters: &nextChar, length: 1) as String
    }
    
    return randomString
}

let y = random(10)

//let x = try JSONSerialization.jsonObject(from: "{\"key\": 1}") as! [String:Any]
//x["key"]

//let y = [2, 2, "A"] as [Any]



//y.sortedArray(withAttribute: nil, ascending: true, ignoreCase: true)
